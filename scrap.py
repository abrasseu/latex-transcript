#!/usr/bin/env python3

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.models import reset_db, Course
from src.utc_specific.scrapper import JSDumpUTCScrapper
from src.utils import get_db_string

if __name__ == "__main__":
    scrapper = JSDumpUTCScrapper()

    # Cleaning things
    reset_db()

    db_string = get_db_string()
    engine = create_engine(db_string, echo=False)
    session_maker = sessionmaker(bind=engine)

    courses, fr_descrs, en_descrs = scrapper.get_courses_and_description()
    courses_codes = list(map(lambda c: c.code, courses))
    for batch in [courses, fr_descrs, en_descrs]:
        session = session_maker()
        session.add_all(batch)
        session.commit()

    # Getting Semesters and Results (online, if the first time)
    html_of_result, _ = scrapper.get_html_results_and_student_details()
    diplomas, semesters, course_results = scrapper.scrap_results(html_of_result)

    for batch in [diplomas, semesters, course_results]:
        session = session_maker()
        session.add_all(batch)
        session.commit()

    # Getting Info about courses online
    perso_courses_codes = list(map(lambda cr: cr.course_code, course_results))

    # Missing courses are courses present in the student profile but not online
    missing_courses_code = set(perso_courses_codes).difference(courses_codes)
    if len(missing_courses_code) != 0:
        print("Missing courses online")
        for m_c in missing_courses_code:
            print(m_c)

        missing_courses = list(map(lambda cc: Course(code=cc), missing_courses_code))
        session = session_maker()
        session.add_all(missing_courses)
        session.commit()
