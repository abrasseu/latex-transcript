import textwrap

from sqlalchemy import (
    create_engine,
    Column,
    Integer,
    String,
    Text,
    Boolean,
    ForeignKey,
    Date,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from .utils import get_db_string

Base = declarative_base()


class Diploma(Base):
    __tablename__ = "diploma"

    short_name = Column(Text, primary_key=True)

    name = Column(Text)

    @hybrid_property
    def start(self):
        return min(sem.start for sem in self.semesters)

    @hybrid_property
    def end(self):
        return max(sem.end for sem in self.semesters)

    @hybrid_property
    def period(self):
        return "{} -- {}".format(
            self.start.strftime("%B %Y"), self.end.strftime("%B %Y")
        )


class Semester(Base):
    __tablename__ = "semester"

    name = Column(String, primary_key=True)

    diploma_short_name = Column(String, ForeignKey("diploma.short_name"))

    start = Column(Date)
    end = Column(Date)
    level = Column(String)
    observation = Column(Text)

    diploma = relationship("Diploma", back_populates="semesters")

    @hybrid_property
    def period(self):
        return "{} {} -- {} {}".format(
            self.first_month, self.year, self.last_month, self.year_end_semester
        )

    @hybrid_property
    def is_fall(self):
        return "A" in self.name

    @hybrid_property
    def type(self):
        return "Automne" if self.is_fall else "Printemps"

    @hybrid_property
    def first_month(self):
        return (
            self.start.strftime("%B")
            .replace("February", "Février")
            .replace("September", "Septembre")
        )

    @hybrid_property
    def last_month(self):
        return (
            self.end.strftime("%B")
            .replace("January", "Janvier")
            .replace("July", "Juillet")
        )

    @hybrid_property
    def code(self):
        return self.name[0] + self.name[3:5]

    @hybrid_property
    def year(self):
        return self.start.strftime("%Y")

    @hybrid_property
    def year_end_semester(self):
        return self.end.strftime("%Y")

    def __repr__(self):
        return "Semester {} : {}".format(self.name, self.level)


class Course(Base):
    __tablename__ = "course"

    code = Column(String, primary_key=True)
    ects = Column(Integer)
    type = Column(String)
    taught_in_spring = Column(Boolean)
    taught_in_fall = Column(Boolean)
    has_final_exam = Column(Boolean)

    def __repr__(self):
        repr = "{} ({})\n".format(self.code, self.type)
        repr += " - worths {} ECTS\n".format(self.ects)
        repr += " - taught in Spring\n" if self.taught_in_spring else ""
        repr += " - taught in Fall\n" if self.taught_in_fall else ""
        repr += " - has a final exam\n" if self.has_final_exam else ""

        return repr


class CourseDescription(Base):
    __tablename__ = "course_description"

    course_code = Column(String, ForeignKey("course.code"), primary_key=True)
    lang = Column(String, primary_key=True)

    title = Column(String)
    overview = Column(Text)
    bibliography = Column(Text)
    recommended_level = Column(String)
    assessment_criteria = Column(String)
    success_criteria = Column(String)
    misc = Column(String)

    training_objectives = Column(Text)
    pedagogical_objectives = Column(Text)
    other_objectives = Column(Text)

    curriculum = Column(Text)
    outcomes = Column(Text)

    course = relationship("Course", back_populates="descriptions")

    def __repr__(self):
        def format_paragraph(repr):
            return "\n".join(textwrap.wrap(repr))

        repr = "Description of {} in {}\n\n".format(self.course_code, self.lang)

        repr += " - Title: {}\n".format(format_paragraph(self.title))
        repr += " - Overview:\n{}\n\n".format(format_paragraph(self.overview))
        repr += " - Curriculum:\n {}\n\n".format(format_paragraph(self.curriculum))
        repr += " - Outcomes:\n {}\n\n".format(format_paragraph(self.outcomes))

        repr += " - Training Objectives:\n {}\n\n".format(
            format_paragraph(self.training_objectives)
        )
        repr += " - Pedagogical Objectives:\n {}\n\n".format(
            format_paragraph(self.pedagogical_objectives)
        )
        repr += " - Other Objectives:\n {}\n\n".format(
            format_paragraph(self.other_objectives)
        )

        repr += " - Bibliography:\n {}\n\n".format(format_paragraph(self.bibliography))
        repr += " - Recommended level:\n {}\n\n".format(
            format_paragraph(self.recommended_level)
        )
        repr += " - Assessment Criteria:\n {}\n\n".format(
            format_paragraph(self.assessment_criteria)
        )
        repr += " - Success Criteria:\n {}\n\n".format(
            format_paragraph(self.success_criteria)
        )
        repr += " - Misc.:\n {}\n".format(format_paragraph(self.misc))
        repr += "\n"

        return repr


class CourseResult(Base):
    __tablename__ = "course_result"

    course_code = Column(String, ForeignKey("course.code"), primary_key=True)
    semester_name = Column(String, ForeignKey("semester.name"), primary_key=True)
    result = Column(String)

    course = relationship("Course", back_populates="results")
    semester = relationship("Semester", back_populates="results")

    def __repr__(self):
        return "{}@{}: {}".format(self.course_code, self.semester_name, self.result)


# Foreign Keys Management

# Semester.diploma_short_name => Diploma.short_name
Diploma.semesters = relationship("Semester", back_populates="diploma")

# CourseDescription.course_code => Course.code
Course.descriptions = relationship(
    "CourseDescription", order_by=Course.code, back_populates="course"
)

# CourseResult.course_code => Course.code
Course.results = relationship(
    "CourseResult", order_by=Course.code, back_populates="course"
)

# CourseResult.semester_name => Semester.name
Semester.results = relationship(
    "CourseResult", order_by=Semester.name, back_populates="semester"
)


def create_tables(verbose=False):
    print("Creating database")
    db_string = get_db_string()
    engine = create_engine(db_string, echo=verbose)
    Base.metadata.create_all(engine)


def reset_db(verbose=False):
    print("Resetting database")
    db_string = get_db_string()
    engine = create_engine(db_string, echo=verbose)
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    print("Database reset")


if __name__ == "__main__":
    create_tables()
