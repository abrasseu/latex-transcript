import os

from sqlalchemy import func

from src.models import Semester, CourseResult, Course, CourseDescription, Diploma
from src.settings import PROJECT_FOLDER
from .utils import get_public_attributes_names, StudentDetails


class LaTeXer:
    """
    An util to generate the pdf given the information.

    Internally convert information into strings that are injected into a .tex
    document that is finally compiled.

    This document is built using 3 templates that specify respectively
    the header, the beginning of the transcript as well as its footer.
    """

    def __init__(
        self,
        lang: str,
        final_doc: str,
        out_dir=os.path.join(PROJECT_FOLDER, "out"),
        templates_dir=os.path.join(PROJECT_FOLDER, "templates"),
    ):
        self._final_doc = final_doc
        self._out_dir = out_dir
        self._main_doc = os.path.join(out_dir, "main.tex")
        self._lang = lang

        # Factorization of code: programmatically setting attributes
        for field in [
            "_preamble",
            "_beg_doc",
            "_end_doc",
            "_semester_template",
            "_course_template",
            "_semester_opts_template",
            "_diploma_template",
        ]:
            file = "{}.tex".format(field[1:])
            with open(os.path.join(templates_dir, self._lang, file), "r") as tex_file:
                setattr(self, field, tex_file.read())

    @staticmethod
    def _inject_in_string(string: str, to_inject: dict):
        """
        Safely inject values in a string with placeholders.
        Dangerous characters or string are discarded or replaced
        in order for the document to be compiled.

        :param string: the string with placeholders
        :param to_inject: a dict like: {"PLACEHOLDER": value … }
        :return: the formatted string
        """
        for key, value in to_inject.items():
            # Replacing dangerous strings for LaTeX
            if value is None:
                value = "\\emptyvalue"
            else:
                value = (
                    str(value)
                    .replace("^", "")
                    .replace("$", "")
                    .replace("&", "\&")
                )

            # Injecting value
            string = string.replace(key, value)
        return string

    def _new_command(self, name, value):
        return self._inject_in_string(
            "\\newcommand{\\NAME}{VALUE}", to_inject={"NAME": name, "VALUE": value}
        )

    def _latexify_student_details(self, student_details: StudentDetails):
        """
        Returns details of a student

        :param student_details:
        :return: LaTeXified lines of StudentDetails
        """
        return [
            self._new_command(name.replace("_", ""), getattr(student_details, name))
            for name in get_public_attributes_names(student_details)
        ]

    def _course_string(
        self,
        course: Course,
        course_description: CourseDescription,
        result: CourseResult,
    ):
        """
        Returns a formatted string in LaTeX of the courses information, results
        and description.

        :param course: a Course object
        :param course_description : a CourseDescription object
        :param result : a CourseResult object
        :return: a string, LaTeX formatted.
        """
        to_inject = {
            "COURSE_CODE": course.code,
            "COURSE_TYPE": course.type,
            "COURSE_TITLE": course_description.title,
            "COURSE_ECTS": course.ects,
            "GRADE_OBTAINED": result.result,
            "COURSE_OVERVIEW": course_description.overview,
            "COURSE_PROGRAM": course_description.curriculum,
        }

        return self._inject_in_string(self._course_template, to_inject)

    def _diploma_string(self, diploma: Diploma):

        to_inject = {"DIPLOMA_NAME": diploma.name, "DIPLOMA_PERIOD": diploma.period}

        return self._inject_in_string(self._diploma_template, to_inject)

    def _semester_string(self, sem: Semester, add_sem_comment: bool = False):

        to_inject = {
            "SEMESTER_DATE": sem.name,
            "SEMESTER_LEVEL": sem.level,
            "SEMESTER_CODE": sem.code,
            "SEMESTER_TYPE": sem.type,
            "SEMESTER_YEAR": sem.year,
            "SEMESTER_PERIOD": sem.period,
        }

        formatted_string = self._inject_in_string(self._semester_template, to_inject)

        if add_sem_comment:
            formatted_string += self._inject_in_string(
                self._semester_opts_template, {"SEMESTER_OBSERVATION": sem.observation}
            )

        return formatted_string

    def _compile_tex_to_pdf(self):
        os.system(
            "pdflatex -halt-on-error -output-directory {} {}".format(
                self._out_dir, self._main_doc
            )
        )

    def generate_transcript(
        self,
        student_details: StudentDetails,
        session,
        from_date=None,
        last_first=True,
        remove_temp_dir=True,
        add_sem_comment=False,
        break_page_on_semester=False,
    ):
        """
        Creates the final .tex file

        :param student_details: the details of the student
        :param session: a session to the database
        :param remove_temp_dir: if True, remove the temporary directory use to
        build the document
        :param add_sem_comment: if True, add the observation got of the semester
        :param break_page_on_semester: if True, insert a page break after each semester
        :return:
        """
        if not (os.path.exists(self._out_dir)):
            os.mkdir(self._out_dir)

        with open(self._main_doc, "w") as out:
            # Prepending the preamble
            out.write(self._preamble)

            # Inject Student details
            latex_student_details = self._latexify_student_details(student_details)

            # Adding total number of credits
            total_nb_credits = (
                session.query(func.sum(Course.ects)).join(Course.results).all()[0][0]
            )
            latex_student_details.append(
                self._new_command("totalcredits", str(total_nb_credits))
            )
            out.writelines("%s\n" % info for info in latex_student_details)

            # Appending the header
            out.write(self._beg_doc)

            # Building the table
            diplomas = session.query(Diploma).all()
            for diploma in diplomas:

                out.write(self._diploma_string(diploma))

                same_diploma = Semester.diploma_short_name == diploma.short_name
                semesters = session.query(Semester).filter(same_diploma)

                if from_date:
                    semesters = semesters.filter(Semester.start > from_date)
                if last_first:
                    semesters = semesters.order_by(Semester.start.desc())

                for semester in semesters:

                    out.write(self._semester_string(semester, add_sem_comment))

                    course_results = session.query(CourseResult).filter(
                        CourseResult.semester_name == semester.name
                    )

                    for result in course_results:
                        course_q = (
                            session.query(Course)
                            .filter(Course.code == result.course_code)
                            .all()
                        )

                        description_q = (
                            session.query(CourseDescription)
                            .filter(CourseDescription.lang == self._lang)
                            .filter(CourseDescription.course_code == result.course_code)
                            .all()
                        )

                        # Soft replacement if nothing is found for the course or its description
                        description = (
                            CourseDescription()
                            if len(description_q) == 0
                            else description_q[0]
                        )
                        course = (
                            Course(code=description.course_code, ects=None, type=None)
                            if len(course_q) == 0
                            else course_q[0]
                        )

                        course_info_latex = self._course_string(
                            course, description, result
                        ).splitlines()

                        # Injecting the courses info
                        out.writelines("%s\n" % l for l in course_info_latex)

                    # To break for a new page on each semester
                    if break_page_on_semester:
                        out.write("\\newpage\n")

            # Appending the end of the doc to the file
            out.write(self._end_doc)

        self._compile_tex_to_pdf()

        # Move pdf
        os.rename(self._main_doc.replace(".tex", ".pdf"), self._final_doc)

        if remove_temp_dir and os.path.exists(self._out_dir):
            for file in os.listdir(self._out_dir):
                os.remove(os.path.join(self._out_dir, file))
            os.rmdir(self._out_dir)

        print("Transcript available: {}".format(self._final_doc))
