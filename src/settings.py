import os

PROJECT_FOLDER = os.path.abspath(
    os.path.join(os.path.realpath(__file__), os.pardir, os.pardir)
)
# Cached data for personal results and courses information and descriptions
DATA_FOLDER = os.path.join(PROJECT_FOLDER, "data")  # not sync with .git by default

STUDENT_DETAILS_FILE = os.path.join(DATA_FOLDER, "student_details.pickle")

# The local data base persisting the data
LOCAL_DATABASE = os.path.join(DATA_FOLDER, "courses_and_results.db")

CREDENTIALS_FILE = os.path.join(PROJECT_FOLDER, ".credentials")
DB_URL_FILE = os.path.join(PROJECT_FOLDER, ".db_url")

# The default lang to use, "en" and "fr" available
LANG = "en"
SUPPORTED_LANGS = ["fr", "en"]
