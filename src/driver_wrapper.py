from selenium.webdriver.chrome import webdriver


class DriverWrapper:
    """
    Wraps selenium.webdriver, exposes a similar API and
    some more convenient methods.
    """

    def __init__(self, driver: webdriver):
        self._d = driver

    def selection_option(self, drop_down, to_find):
        """
        Clicks on the first element of a `drop_down` menu
        containing the text `to_find`.

        If such a text isn't present, it does nothing.

        :param drop_down: the drop down menu
        :param to_find: the text to find
        """
        el = self._d.find_element_by_id(drop_down)
        for option in el.find_elements_by_tag_name("option"):
            if option.text == to_find:
                option.click()
                break

    def select_by_tag(self, tag, text):
        """
        Returns the first element of a specific `tag`
        containing the `text`.

        :param driver: the webdriver to use
        :param tag: the tag of the element
        :param text: the text to find
        """
        elems = self._d.find_elements_by_tag_name(tag)
        elems_text = list(map(lambda x: x.text, elems))
        index = elems_text.index(text)

        return elems[index]

    def select_by_class_name(self, class_name, text):
        """
        Returns the first element of a specific `class_name`
        containing the `text`.

        :param driver: the webdriver to use
        :param class_name: the class name of the element
        :param text: the text to find
        """
        elems = self._d.find_elements_by_class_name(class_name)
        elems_text = list(map(lambda x: x.text, elems))
        index = elems_text.index(text)

        return elems[index]

    def get_text(self, xpath):
        """
        Returns the text of a HTML element based using a xpath.

        :param driver: the driver to use
        :param xpath: the xpath of the element
        :return: the text as a string
        """
        return self._d.find_element_by_xpath(xpath).text.strip()

    # selenium.webdriver API

    def get(self, url):
        return self._d.get(url)

    def find_element_by_name(self, name):
        return self._d.find_element_by_name(name)

    def find_element_by_id(self, name):
        return self._d.find_element_by_id(name)

    def title(self):
        return self._d.title

    def page_source(self):
        return self._d.page_source

    def switch_to(self):
        return self._d.switch_to

    def window(self):
        return self._d.window()

    def window_handles(self, num):
        return self._d.window_handles[num]

    def close(self):
        return self._d.close()

    def find_element_by_xpath(self, xpath):
        return self._d.find_element_by_xpath(xpath)

    def maximize_window(self):
        return self._d.maximize_window()

    def find_elements_by_class_name(self, name):
        return self._d.find_elements_by_class_name(name)

    def find_element_by_tag_name(self, name):
        return self._d.find_element_by_tag_name(name)

    def find_elements_by_tag_name(self, name):
        return self._d.find_elements_by_tag_name(name)

    def find_elements_by_xpath(self, name):
        return self._d.find_elements_by_xpath(name)
