import pickle
import types

import os

from .settings import LOCAL_DATABASE, STUDENT_DETAILS_FILE, DB_URL_FILE


def get_public_attributes_names(obj, include_methods_name=False):
    """
    Return the list of public attributes' names of a given object.
    Doesn't return public methods' names by default.

    :param obj: the object to use
    :param include_methods_name : If True, include the methods names in the list.
    :return: a list of string containing the attributes' names.
    """

    def condition_on_methods(name):
        return include_methods_name or not (
            isinstance(getattr(obj, name), types.MethodType)
        )

    return [name for name in dir(obj) if name[0] != "_" and condition_on_methods(name)]


def print_object_public_attributes(obj):
    for attr in get_public_attributes_names(obj):
        print("%s = %r" % (attr, getattr(obj, attr)))


def build_object(obj):
    """
    Incrementally modifies an object asking the user to modify
    the objects attributes or not.

    :param obj: an object
    :return: the same (modified) object
    """
    attr_names = get_public_attributes_names(obj)
    for attr_name in attr_names:
        default_value = getattr(obj, attr_name)
        pretty_attr_name = attr_name.replace("_", " ").capitalize()
        new_value = input(
            "{} (current value : {}) : ".format(pretty_attr_name, default_value)
        )
        if new_value != "":
            setattr(obj, attr_name, new_value)
    return obj


def get_db_string(force_local=False, database_url_file: str = DB_URL_FILE):
    """
    Returns the url of the database to use based on the configuration.

    If the URL is not present in the configuration, a local SQLite database
    will be used.

    :param force_local: If True, will return the database string of the local SQLite database.
    :param database_url_file:
    :return:
    """

    if not force_local and os.path.exists(database_url_file):
        file = open(database_url_file, "r")
        content = file.readline()
        file.close()
        parsed = content.replace('"', "").replace("\n", "")
        if len(parsed) > 0:
            print("Using remote database")
            return parsed

    db_string = "sqlite:///" + LOCAL_DATABASE
    print("Using local SQLite database : {}".format(db_string))
    parent_dir = os.path.dirname(database_url_file)
    os.makedirs(parent_dir, exist_ok=True)
    return db_string


class StudentDetails:
    """
    A wrapper class for the student info.

    Such an object will be persisted using pickle in the `data_folder`
    and will be used to format the final old.tex using latex_dump.
    """

    def __init__(self):
        self.surname = "Dupont"
        self.name = "Jean"
        self.given_names = "Jean Paul Georges"
        self.birthday = "10/12/1940"
        self.place_birth = "Paris"
        self.student_number = "1337"

        self.university = "University of Technology of Compiègne"
        self.country = "France"
        self.city = "Paris"
        self.diploma = "Ms. Computer Science & Engineering"
        self.diploma_short = "Ms. CS & Eng."
        self.major = "CS"
        self.minor = "Maths"

        self.enrolled_since = "September 1960"
        self.study_end = "June 1965"


def get_student_details(file: str = STUDENT_DETAILS_FILE):
    """
    Returns the student details.

    If it doesn't exist, builds it incrementally.

    :return: the persisted StudentDetails.
    """
    try:
        student_details = pickle.load(open(file, "rb"))
    except FileNotFoundError:

        print("Student details not present : creating them")
        student_details = build_object(StudentDetails())

        pickle.dump(student_details, open(file, "wb"))

    return student_details
