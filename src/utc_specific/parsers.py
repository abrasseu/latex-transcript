from bs4 import BeautifulSoup

from src.models import Course, CourseDescription


class UTCXMLParser:
    """ A parser to extract information from the json file.
    For UTC students only.

    Subclass are language specific.
    """

    def parse(self, course_xml_lang):
        """
        Parse the XML corresponding to a course with the description
        being given in the specific langage.

        :return:
        """

        # Course Default values
        type_ = None
        ects = 0
        taught_in_fall = False
        taught_in_spring = False
        has_final_exam = False

        course_descr_content = {k: "" for k in self.course_description_headers.keys()}

        soup = BeautifulSoup(course_xml_lang, "lxml")
        code_title = soup.h4.text.strip()
        code, title = code_title.split(":", 1)

        code = code.strip()
        title = title.strip().capitalize()
        course_descr_content["title"] = title
        course_descr_content["course_code"] = code
        course_descr_content["lang"] = self.lang

        # Doing some gymnastic 🤸
        # Data is contained in a table, parsing it is not trivial
        tr_entries = soup.find_all("td")
        for entry in tr_entries:
            header = entry.text
            if self.ects_text in header:
                associated_text = entry.next_sibling.next_sibling.text
                ects = int(associated_text.replace("ECTS", "").strip())
            elif self.taugh_in_text in header:
                associated_text = entry.next_sibling.next_sibling.text
                taught_in_fall = self.automn_text in associated_text
                taught_in_spring = self.spring_text in associated_text
            elif self.has_final_exam_text == header:
                has_final_exam_ans = entry.next_sibling.next_sibling
                has_final_exam = self.yes_text in has_final_exam_ans

            # Category
            elif self.cs_text in header:
                type_ = "CS"
            elif self.tm_text in header:
                type_ = "TM"
            elif self.tsh_text in header:
                type_ = "TSH"

            # Course Description parsing
            for field, h in self.course_description_headers.items():
                if h in header:
                    try:
                        associated_text = entry.next_sibling.next_sibling.text
                        course_descr_content[field] = associated_text
                    except AttributeError as e:
                        print("{} thrown while parsing".format(type(e)))
                        print(" - UV: {}".format(code))
                        print(" - Entry: {}".format(entry))
                        print(" - Field: {}".format(field))
                        print(e)

        course = Course(
            code=code,
            type=type_,
            ects=ects,
            taught_in_fall=taught_in_fall,
            taught_in_spring=taught_in_spring,
            has_final_exam=has_final_exam,
        )

        course_descr = CourseDescription(**course_descr_content)

        return course, course_descr


class FrenchParser(UTCXMLParser):
    def __init__(self):
        self.lang = "fr"

        # Text used to find specific field
        self.ects_text = "Crédits : "
        self.taugh_in_text = "Enseignée en : "
        self.has_final_exam_text = "Examen final : "
        self.cs_text = "Scientifiques (CS-ST)"
        self.tm_text = "Techniques (TM-ST)"
        self.tsh_text = "Technologie et Sciences de l'Homme"
        self.yes_text = "Oui"
        self.automn_text = "Automne"
        self.spring_text = "Printemps"

        self.course_description_headers = {
            "overview": "Description brève : ",
            "bibliography": "Ouvrage(s) de référence : ",
            "recommended_level": "Niveau conseillé : ",
            "assessment_criteria": "Conditions d'évaluation : ",
            "success_criteria": "Conditions d'attribution : ",
            "misc": "Particularités: ",
            "training_objectives": "Objectif de formation : ",
            "pedagogical_objectives": "Objectifs pédagogiques spécifiques : ",
            "other_objectives": "Objectifs pédagogiques transverses : ",
            "curriculum": "Programme : ",
            "outcomes": "Résultats : ",
        }


class EnglishParser(UTCXMLParser):
    def __init__(self):
        self.lang = "en"

        # Text used to find specific field
        self.ects_text = "Credits : "
        self.taugh_in_text = "Semester(s) : "
        self.has_final_exam_text = "Final exam : "
        self.cs_text = "Scientifiques (CS-ST)"
        self.tm_text = "Techniques (TM-ST)"
        self.tsh_text = "Technologie et Sciences de l'Homme"
        self.yes_text = "Oui"
        self.automn_text = "Automne"
        self.spring_text = "Printemps"

        self.course_description_headers = {
            "overview": "Short description : ",
            "bibliography": "Reference work : ",
            "recommended_level": "Required level : ",
            "assessment_criteria": "Evaluation : ",
            "success_criteria": "Conditions for providing : ",
            "misc": "Particularities: ",
            "training_objectives": "Course objective : ",
            "pedagogical_objectives": "Specific pedagogical objectives : ",
            "other_objectives": "Transversal specific objectives : ",
            "curriculum": "Program : ",
            "outcomes": "Results : ",
        }
