import codecs
import datetime
import time
import getpass
import json
import os
import pickle
from collections import deque

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from src.driver_wrapper import DriverWrapper
from src.models import CourseResult, Semester, Diploma
from src.settings import (
    DATA_FOLDER,
    STUDENT_DETAILS_FILE,
    CREDENTIALS_FILE,
)
from src.utc_specific.parsers import EnglishParser, FrenchParser
from src.utils import build_object, StudentDetails, get_student_details


class UTCScrapper:
    def __init__(
        self,
        results_file: str = os.path.join(DATA_FOLDER, "results.html"),
        student_details_file: str = STUDENT_DETAILS_FILE,
        credentials_file: str = CREDENTIALS_FILE,
    ):
        self._results_file = results_file
        self._student_details_file = student_details_file
        self._credentials_file = credentials_file
        self._encoding = "ISO-8859-15"

        self._ent_url = "https://webapplis.utc.fr/ent/index.jsf"

        self._short_name_to_diploma = {
            "TC": "Two First Preparatory Years",
            "GI": "Ms. Computer Science & Engineering",
            "GM": "Ms. Mechanical Engineering",
            "GSM": "Ms. Mechanical Engineering",
            "IM": "Ms. Mechanical Engineering",
            "GSU": "Ms. Urban Engineering",
            "GU": "Ms. Urban Engineering",
            "GB": "Ms. Biotechnology",
            "GP": "Ms. Process Engineering",
            "HT": "Bs. Humanities & Technology",
        }

    def export_to_file(self, html, filename, encoding):
        """
        Exports HTML to file and returns it as a string

        :param html: the webdriver on the page
        :param filename: filename to use
        :return: the html as a string
        """
        file_object = codecs.open(filename, "w", encoding)
        file_object.write(html)
        file_object.close()

        return html

    def get_html_results_and_student_details(self):
        """
        Returns the html file of the student's results.

        Saves it if not present.

        :return: an string of the html
        """
        if not (os.path.exists(self._results_file)) or not (
            os.path.exists(self._student_details_file)
        ):
            print("Results or student details not present, getting them now !")

            driver = DriverWrapper(webdriver.Chrome())
            driver.get(self._ent_url)

            # Connecting using the CAS
            if "CAS" in driver.title():
                if os.path.exists(self._credentials_file):
                    with open(self._credentials_file, "r") as cred_file:
                        login, password = cred_file.read().split("\n")
                else:
                    print("{} not present".format(self._credentials_file))
                    print("Please enter your credentials")
                    login = input("Login : ")
                    password = getpass.getpass()

                input_login = driver.find_element_by_name("username")
                input_pswd = driver.find_element_by_name("password")
                input_login.clear()
                input_pswd.clear()
                input_login.send_keys(login)
                input_pswd.send_keys(password)
                del login, password
                input_pswd.send_keys(Keys.RETURN)
                assert "MAUVAIS IDENTIFIANT" not in driver.page_source()

            DriverWrapper.select_by_class_name(
                driver, "col-sm-4", "MON DOSSIER ÉTUDIANT"
            ).click()

            # Focus on the new opened tab
            driver.switch_to().window(driver.window_handles(1))

            def switch_to_domaine(domaine_name):
                """
                Switch between the different domains of the frame
                system of the ENT

                :param domaine_name:
                :return:
                """
                driver.switch_to().default_content()
                driver.switch_to().frame(driver.find_element_by_name("menu"))
                driver.find_element_by_id(domaine_name).click()
                driver.switch_to().default_content()
                driver.switch_to().frame(driver.find_element_by_name("frame_contenu"))
                # The ENT is a mess, it needs an awful lot of time to load a tiny html
                # page, so we need to wait a bit for the frame to come to life
                time.sleep(10)

            # Switching between frames
            driver.switch_to().frame(driver.find_element_by_name("menu"))

            # Getting general student info
            student_details = StudentDetails()

            titre_elems = driver.find_elements_by_class_name("titre")
            student_details.given_names = titre_elems[0].text
            names = student_details.given_names.split()
            student_details.name = names[0]
            student_details.surname = names[-1]
            student_details.student_number = titre_elems[1].text

            # To "Etudiant"
            switch_to_domaine("domaine_5")
            marge130 = driver.find_elements_by_class_name("marge130")
            student_details.place_birth = marge130[4].text.capitalize()
            student_details.birthday = marge130[5].text

            # Back to "Jury de suivis"
            def get_results_table(driver):
                """
                Get the table that contains "Observations / Informations" is a bit more
                resilient to change. Still crappy thought
                :return:
                """
                tables = driver.find_elements_by_tag_name("table")
                for table in tables:
                    if "Observations / Informations" in table.text:
                        return table

                raise RuntimeError("Table 'Observations / Informations' not found")

            switch_to_domaine("domaine_3")
            first_semester_tr = (
                get_results_table(driver).find_elements_by_tag_name("tr")[2].text
            )
            type_sem = first_semester_tr[0]  # A or P
            year = int(first_semester_tr[1:5])

            if "A" in type_sem:
                student_details.enrolled_since = "September {}".format(year)
                student_details.study_end = "July {}".format(year + 5)
            else:
                student_details.enrolled_since = "February {}".format(year)
                student_details.study_end = "January {}".format(year + 5)

            print("Some student info have been found ; please correct accordingly")
            build_object(student_details)

            pickle.dump(student_details, open(self._student_details_file, "wb"))

            # Saving the courses results in html
            switch_to_domaine("domaine_3")

            print("Saving results in {} !".format(self._results_file))
            self.export_to_file(
                driver.page_source(), self._results_file, encoding=self._encoding
            )
            driver.close()

        with open(self._results_file, "r", encoding=self._encoding) as file:
            html = file.read()

        student_details = get_student_details(self._student_details_file)
        return html, student_details

    def scrap_results(self, html_result):
        """
        Scrap the results of the html page of personal results and
        return list of associated objects: Semesters, and CourseResults

        The table of results is just really ugly, thus the number
        of fixtures and voodoo values to parse it.

        :param html_result: the html page as a string
        :return: semesters, course_results
        """
        soup = BeautifulSoup(html_result, "html.parser")

        def get_results_table():
            """
            Get the table that contains "Observations / Informations" is a bit more
            resilient to change. Still crappy thought
            :return:
            """
            tables = soup.find_all("table")
            for table in tables:
                if table.find(text="Observations / Informations"):
                    return table

            raise RuntimeError("Table 'Observations / Informations' not found")

        course_table = get_results_table()

        def get_in_html_tree(html_elem, indices_in_tree):
            """
            Recursive fixture to parse html table.

            Walk the table from html_elem with the next index.
            :param html_elem: the current element
            :param indices_in_tree: indices of consecutive elements
            in the table
            :return: the last html_elem defined by the indices
            """
            indices_deque = deque(indices_in_tree)
            if len(indices_deque) == 0:
                return html_elem
            else:
                index = indices_deque.popleft()
                return get_in_html_tree(html_elem.contents[index], indices_deque)

        def get_html_semester(index):
            return get_in_html_tree(course_table, [1, 2 * index + 2])

        type_course = {"CS": 3, "TM": 5, "TSH": 7, "ST": 9}

        def html_course_of_semester(type, sem, course_index):
            return get_in_html_tree(sem, [type_course[type], 1 + 2 * course_index])

        def course_result_from_html(html_course, semester_name):
            content = list(filter(lambda x: x != "" and "\t" not in x, html_course.text.split("\n")))
            course_code = content[0].strip()

            if "TX" in course_code or "PR" in course_code:
                course_code = course_code[0:3] + course_code[-1]

            result = content[1].strip()

            return CourseResult(
                course_code=course_code, semester_name=semester_name, result=result
            )

        def semester_from_html(html_sem):
            name = str(get_in_html_tree(html_sem, [1, 0]))
            level = str(get_in_html_tree(html_sem, [1, 2]))
            observation = str(get_in_html_tree(html_sem, [11, 1]).text)
            diploma_short_name = "".join([char for char in level if not char.isdigit()])

            type_ = name[0]
            year = int(name[1:])

            # Starting and ending date of the semesters, the day may not be exact
            start = (
                datetime.date(year, 9, 4) if type_ == "A" else datetime.date(year, 2, 27)
            )
            end = (
                datetime.date(year + 1, 1, 30)
                if type_ == "A"
                else datetime.date(year, 7, 13)
            )

            if diploma_short_name not in self._short_name_to_diploma.keys():
                raise RuntimeError(
                    "{} is not a known diploma short name".format(diploma_short_name)
                )

            return Semester(
                diploma_short_name=diploma_short_name,
                name=name,
                start=start,
                end=end,
                level=level,
                observation=observation,
            )

        # List to be returned
        diplomas = []
        semesters = []
        course_results = []

        # This double context for Exception is as ugly as the table to parse 🤷
        # No easy way to deduce the structure of it: we, by default, iterate on
        # 14 semesters and for each semester on the 4 categories trying to catch
        # at max 5 UVs.
        try:
            for sem_index in range(14):
                html_sem = get_html_semester(sem_index)
                sem = semester_from_html(html_sem)
                semesters.append(sem)
                print("⋅ Scrapping Semester {}".format(sem.name))
                for type_ in type_course.keys():
                    try:
                        for course_index in range(5):
                            html_course = html_course_of_semester(
                                type_, html_sem, course_index
                            )
                            course_result = course_result_from_html(
                                html_course, sem.name
                            )
                            print("  → ", course_result)
                            course_results.append(course_result)
                    except IndexError:
                        pass
                print("")
        except IndexError:
            pass

        print("Done Scrapping Semesters and Courses !")

        diplomas_short_names = set(map(lambda sem: sem.diploma_short_name, semesters))

        for dsn in diplomas_short_names:
            diplomas.append(
                Diploma(short_name=dsn, name=self._short_name_to_diploma[dsn])
            )

        return diplomas, semesters, course_results

    def get_courses_and_description(self, lang=None, courses_to_scrap=None):
        """
        Get the descriptions of the courses and return associated objects.

        If courses_to_scrap is None, all the courses are scrapped.

        :param lang: the lang to use to scrap the courses
        :param courses_to_scrap: list of the name the courses to scrap as strings
        :return: courses: list of Courses,
                 courses_descriptions: list of CoursesDescriptions,
                 missing_courses_code: list of code of missing courses
        """
        raise NotImplementedError()


class JSDumpUTCScrapper(UTCScrapper):
    """ To get the descriptions of the courses using the JavaScript snippet. """

    def __init__(
        self,
        js_dump=os.path.join(DATA_FOLDER, "data.json"),
        results_file: str = os.path.join(DATA_FOLDER, "results.html"),
        student_details_file: str = STUDENT_DETAILS_FILE,
        credentials_file: str = CREDENTIALS_FILE,
    ):
        super(JSDumpUTCScrapper, self).__init__(
            results_file, student_details_file, credentials_file
        )
        self._js_dump = js_dump

    def get_courses_and_description(self, lang=None, courses_to_scrap=None):
        with open(self._js_dump) as f:
            data = json.load(f)

        # Cleaning the json
        data = {
            k: {kk: vv.replace("\t", "") for kk, vv in v.items()}
            for k, v in data.items()
        }

        fr_parser = FrenchParser()
        en_parser = EnglishParser()

        courses = []
        descrs = []
        for uv_code in data.keys():
            fr_xml = data[uv_code]["fr"]
            en_xml = data[uv_code]["en"]
            course, fr_descr = fr_parser.parse(fr_xml)
            _, en_descr = en_parser.parse(en_xml)
            courses.append(course)
            descrs.append(fr_descr)
            descrs.append(en_descr)

        return courses, descrs, []
