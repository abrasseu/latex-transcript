#!/usr/bin/env python3
import os
import argparse

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.latexer import LaTeXer
from src.settings import PROJECT_FOLDER
from src.utils import get_db_string, get_student_details


def build_pdf(lang: str = "fr", out: str = "main.pdf", **kwargs) -> None:
    # Specifying info
    final_doc = os.path.join(PROJECT_FOLDER, out)
    latexer = LaTeXer(lang=lang, final_doc=final_doc)

    # Getting interface for data
    engine = create_engine(get_db_string())
    session_maker = sessionmaker(engine)
    session = session_maker()

    student_details = get_student_details()

    # Cooking the transcript 👨‍🍳
    latexer.generate_transcript(student_details, session, **kwargs)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--lang",
        default="fr",
        help="The language to build the transcript to.")
    parser.add_argument(
        "--out",
        default="main.pdf",
        help="The name of the final file in the output folder.")
    parser.add_argument(
        "--from_date",
        default=None,
        help="If specified only the semesters from this date will be printed.")
    parser.add_argument(
        "--last_first",
        action="store_true",
        help="Print last semesters first.")
    parser.add_argument(
        "--remove_temp_dir",
        action="store_true",
        help="Remove temporary directory.")
    parser.add_argument(
        "--add_sem_comment",
        action="store_true",
        help="Add semesters observations")
    parser.add_argument(
        "--break_page_on_semester",
        action="store_true",
        help="Start a new page for each semester")

    kwargs = vars(parser.parse_args())
    build_pdf(**kwargs)
