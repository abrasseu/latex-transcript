Enhanced Transcript of Records 📖 ☑
====================

[![pipeline status](https://gitlab.utc.fr/jjerphan/Enhanced-Transcript/badges/master/pipeline.svg)](https://gitlab.utc.fr/jjerphan/Enhanced-Transcript/commits/master)

Generate a nice transcript of records automatically for you 👌

![e](./docs/output_extract.png)

## Setting up the project:


 - Install Chrome or Chromium


 - Create a new virtual environment for python 3 using under the `venv` directory:
 > ⚠ Python 2.7 is not supported and won't be supported.

```bash
$ cd Enhanced-Transcript/
$ python -m venv venv
$ source venv/bin/activate
```

- Install the dependencies

```bash
$ pip install -r requirements.txt
```

- Download `chromedriver` (it is needed by `selenium` to scrap data):
    - If on Linux or OSX, you can use your package manager for that.
    - Alternatively, you can [do it manually](](http://chromedriver.chromium.org/downloads)): put it in the `venv/bin/`  folder. . On Linux:

    ```bash
    wget https://chromedriver.storage.googleapis.com/<version_number>/chromedriver_<your_os>.zip
    unzip chromedriver_<your_os>.zip
    FOLDER=$(dirname `which python`)
    mv chromedriver $FOLDER
    rm chromedriver_<your_os>.zip
    ```

## Usage

### As a student from the University of Technology of Compiègne 🍺

After having setting up the project (see above):

- Scrap the data ; on the first time, it will ask to enter your credentials to get your data. 

```bash
./scrap.py
```
> ⚠ Here you will need the `data/data.json` file in order for the courses descriptions to be created. I have this file but I did not put it online because it is big. **Ask me for it!**

- *Some details about the previous step:* after this completes, all the data regarding your results and the courses will be persisted in a local SQLite database (by default `data/courses_and_results.db`). As for your personnal student details, they will be stored in a `.pickle` file (by default `data/student_details.pickle`).


- Build your transcript as a pdf file:

```bash
./build_pdf.py
```

Annnnd you're done ! 🙃

The document is output as `./main.pdf`.

### As another student from another university in the 🗺

You have to get your course data yourself. Then, you can use the database schema defined in `models.py` to format it and then use the latexer to output your document:

```bash
./build_pdf.py
```

## Source Code Structure

This project is organised in (simplistic) modules:

- `driver_wrapper.py`: a small wrapper around Selenium's
- `experiments.py`: experiments that you should not try at home ⚠
- `latexer.py`: defines a `LaTeXer` that creates the final transcript using the data
- `models.py`: defines the schema of the data to be persisted (see bellow)
- `settings.py`:  various settings about paths
- `utils.py`: various functions 

## Data Schema

Data gets persisted in a local SQLite database under this schema.

![UML](./docs/uml.png)

### Using your own database:

You can use your own data base (PostGRESQL, MySQL, WhateverSQL…) if you want!
To do this, just put `database_url` in a file named `.db_url` ( see `.db_url.dist` for the format to use).

## About the template and this project license

To generate the final document, we used a modified version of [this template](https://itp.uni-frankfurt.de/~guterding/misc/Transcript.tex) from [muellis](https://blogs.gnome.org/muelli/) (see [here](https://blogs.gnome.org/muelli/2009/06/latex-transcript-of-records/)).

This project license is MIT.
